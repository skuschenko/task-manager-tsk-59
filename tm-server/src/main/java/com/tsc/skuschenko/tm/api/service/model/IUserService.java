package com.tsc.skuschenko.tm.api.service.model;

import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserService {

    void addAll(@Nullable List<User> users);

    void clear();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    @SneakyThrows
    List<User> findAll();

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User findById(@Nullable String email);

    @Nullable
    User findByLogin(@Nullable String login);

    boolean isEmailExist(@Nullable String login);

    boolean isLoginExist(@Nullable String login);

    @NotNull
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User removeByLogin(@Nullable String login);

    @NotNull
    User setPassword(@Nullable String userId, @Nullable String password);

    @NotNull
    User unlockUserByLogin(@Nullable String login);

    @NotNull
    User updateUser(
            @Nullable String userId, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );

}
