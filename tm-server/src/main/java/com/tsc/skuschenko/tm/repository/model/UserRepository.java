package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.api.repository.model.IUserRepository;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import com.tsc.skuschenko.tm.model.User;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

   /* public UserRepository(@NotNull @Autowired EntityManager entityManager) {
        this.entityManager = entityManager;
    }*/

    @Override
    public void clear() {
        @NotNull final String query =
                "DELETE FROM User e";
        entityManager.createQuery(query, User.class).executeUpdate();
    }

    @Override
    public @Nullable List<User> findAll() {
        @NotNull final String query =
                "SELECT e FROM User e ";
        return entityManager.createQuery(query, User.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String query =
                "SELECT e FROM User e WHERE e.email = :email";
        TypedQuery<User> typedQuery =
                entityManager.createQuery(query, User.class)
                        .setParameter("email", email);
        return getEntity(typedQuery);
    }

    @Override
    public @Nullable User findById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query =
                "SELECT e FROM User e WHERE e.login = :login";
        TypedQuery<User> typedQuery =
                entityManager.createQuery(query, User.class)
                        .setParameter("login", login);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public User getReference(@NotNull final String id) {
        return entityManager.getReference(User.class, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User lockUserByLogin(@NotNull final String login) {
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        update(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        Optional.ofNullable(user).ifPresent(this::remove);
        return user;
    }

    @Override
    public void removeOneById(@NotNull String id) {
        @NotNull final String query =
                "DELETE FROM User e WHERE e.id = :id";
        entityManager.createQuery(query)
                .setParameter("id", id)
                .executeUpdate();
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(
            @NotNull final String userId, @NotNull final String password
    ) {
        @NotNull final User user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(password);
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User unlockUserByLogin(@NotNull final String login) {
        @NotNull final User user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        update(user);
        return user;
    }

}
