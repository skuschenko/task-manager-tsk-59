package com.tsc.skuschenko.tm.api.entity;

import org.jetbrains.annotations.NotNull;

public interface IHasName {

    @NotNull
    String getName();

    void setName(String name);

}
