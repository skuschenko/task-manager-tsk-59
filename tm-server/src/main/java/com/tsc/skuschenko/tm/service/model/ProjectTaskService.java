package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.repository.model.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.model.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.model.IProjectTaskService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.service.AbstractService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public final class ProjectTaskService extends AbstractService
        implements IProjectTaskService {

    @NotNull
    private final String PROJECT_ID = "project id";

    @NotNull
    private final String TASK_ID = "task id";

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public Task bindTaskByProject(
            @NotNull final String userId, @Nullable final Project project,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(project)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        Optional.ofNullable(projectRepository.findOneById(
                userId, project.getId()
                )
        )
                .orElseThrow(ProjectNotFoundException::new);
        @NotNull final Task task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProject(project);
        taskRepository.update(task);
        return task;
    }

    @Override
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<Project> projects =
                projectRepository.findAllWithUserId(userId);
        @Nullable final Optional<List<Project>> findProjects =
                Optional.ofNullable(projects).filter(item -> item.size() != 0);
        findProjects.ifPresent(items -> items.forEach(item ->
                deleteProjectById(userId, item.getId())));
    }

    @Nullable
    @Override
    @Transactional
    public Project deleteProjectById(
            @NotNull final String userId, @Nullable final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        @Nullable final List<Task> projectTasks =
                findAllTaskByProjectId(userId, projectId);
        @Nullable final Optional<List<Task>> tasks =
                Optional.ofNullable(projectTasks)
                        .filter(item -> item.size() != 0);
        tasks.ifPresent(items -> items.forEach(taskRepository::remove));
        @Nullable final Project project =
                projectRepository.findOneById(userId, projectId);
        projectRepository.removeOneById(userId, projectId);
        return project;
    }

    @Nullable
    @Override
    public List<Task> findAllTaskByProjectId(
            @NotNull final String userId, @Nullable final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @NotNull final String userId, @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                .orElseThrow(ProjectNotFoundException::new);
        @NotNull final Task task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProject(null);
        taskRepository.update(task);
        return task;
    }

}
