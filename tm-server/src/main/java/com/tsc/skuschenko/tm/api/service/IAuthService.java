package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    void checkRoles(@Nullable final Role... roles);

    @Nullable
    UserDTO getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    UserDTO registry(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

}
