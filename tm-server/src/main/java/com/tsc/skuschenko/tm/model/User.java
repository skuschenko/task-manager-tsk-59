package com.tsc.skuschenko.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.listener.EntityListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_user")
@EntityListeners(EntityListener.class)
public final class User extends AbstractEntity {

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column(name = "locked")
    private boolean isLocked = false;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    @Nullable
    private List<Project> projects = new ArrayList<>();

    @Column
    @Nullable
    private String role = Role.USER.getDisplayName();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    @Nullable
    private List<Session> sessions = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    @Nullable
    private List<Task> tasks = new ArrayList<>();

}
