package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.enumerated.EntityOperationType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractWrapper implements Serializable {

    @Nullable
    private String className;

    @Nullable
    private String date;

    @Nullable
    private String entity;

    @Nullable
    private EntityOperationType operation;

}
