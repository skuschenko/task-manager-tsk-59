package com.tsc.skuschenko.tm.service.model;

import com.tsc.skuschenko.tm.api.repository.model.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.dto.ITaskService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.repository.model.TaskRepository;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
public final class TaskService extends AbstractBusinessService<Task>
        implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task add(
            @Nullable final User user, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void addAll(@Nullable final List<Task> tasks) {
        Optional.ofNullable(tasks).ifPresent(
                items -> items.forEach(
                        item -> add(
                                item.getUser(),
                                item.getName(),
                                item.getDescription())
                )
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(TaskNotFoundException::new);
        task.setStatus(status.getDisplayName());
        task.setStatus(status.getDisplayName());
        taskRepository.update(task);
        return task;

    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        taskRepository.clearAllTasks();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@NotNull final String userId) {
        taskRepository.clear(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneById(userId, id))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Task task =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        taskRepository.update(task);
        return task;

    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task completeByName(@NotNull final String userId,
                               @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.COMPLETE.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<Task> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final ITaskRepository taskRepository =
                context.getBean(TaskRepository.class, entityManager);
        return taskRepository.findAllWithUserId(userId)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll(@NotNull final String userId) {
        return taskRepository.findAllWithUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return taskRepository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return taskRepository.findOneByName(userId, name);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final Task task =
                Optional.ofNullable(taskRepository.getReference(id))
                        .orElseThrow(TaskNotFoundException::new);
        taskRepository.remove(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Task task =
                Optional.ofNullable(findOneByIndex(userId, index))
                        .orElseThrow(TaskNotFoundException::new);
        taskRepository.remove(
                taskRepository.getReference(task.getId())
        );
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task =
                Optional.ofNullable(findOneByName(userId, name))
                        .orElseThrow(TaskNotFoundException::new);
        taskRepository.remove(
                taskRepository.getReference(task.getId())
        );
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        @NotNull final ITaskRepository taskRepository =
                context.getBean(TaskRepository.class, entityManager);
        @NotNull final Task task = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final Task task = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task = Optional.ofNullable(
                findOneByName(userId, name)
        ).orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskRepository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Task updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final Task task = Optional.ofNullable(
                findOneByIndex(userId, index)
        ).orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        taskRepository.update(task);
        return task;
    }

}