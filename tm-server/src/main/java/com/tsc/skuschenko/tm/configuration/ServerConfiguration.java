package com.tsc.skuschenko.tm.configuration;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.exception.system.ConnectionFailedException;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Optional;
import java.util.Properties;

@EnableTransactionManagement
@ComponentScan("com.tsc.skuschenko.tm")
public class ServerConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    private static final String LITE_MEMBER =
            "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    @Bean
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource =
                new DriverManagerDataSource();
        @Nullable final String dataSourceDriver=propertyService.getJdbcDriver();
        @Nullable final String dataSourceUrl=propertyService.getJdbcUrl();
        @Nullable final String dataSourceUser=propertyService.getJdbcUserName();
        @Nullable  final String dataSourcePassword =
                propertyService.getJdbcPassword();
        Optional.ofNullable(dataSourceDriver).orElseThrow(() ->
                new ConnectionFailedException("Driver")
        );
        Optional.ofNullable(dataSourceUrl).orElseThrow(() ->
                new ConnectionFailedException("URL")
        );
        Optional.ofNullable(dataSourceUser).orElseThrow(() ->
                new ConnectionFailedException("User")
        );
        Optional.ofNullable(dataSourcePassword).orElseThrow(() ->
                new ConnectionFailedException("Password")
        );
        dataSource.setDriverClassName(dataSourceDriver);
        dataSource.setUrl(dataSourceUrl);
        dataSource.setUsername(dataSourceUser);
        dataSource.setPassword(dataSourcePassword);
        return dataSource;
    }

    @NotNull
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull final Properties properties
    ) {
        final LocalContainerEntityManagerFactoryBean factoryBean;
        factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                "com.tsc.skuschenko.tm.model",
                "com.tsc.skuschenko.tm.dto");
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @NotNull
    @Bean
    private Properties properties() {
        @Nullable final String factoryClass =propertyService.getFactoryClass();
        @Nullable final String dialect = propertyService.getJdbcDialect();
        @Nullable final String hbm2ddl = propertyService.getJdbcHbm2ddl();
        @Nullable final String showSql = propertyService.getJdbcShowSql();
        @Nullable final String liteMember = propertyService.getLiteMember();
        @Nullable final String minimalPuts = propertyService.getMinimalPuts();
        @Nullable final String providerConfiguration
                = propertyService.getProviderConfiguration();
        @Nullable final String queryCache = propertyService.getQueryCache();
        @Nullable final String regionPrefix = propertyService.getRegionPrefix();
        @Nullable final String secondLevel = propertyService.getSecondLevel();
        @Nullable final String needSecondLevel
                = propertyService.getNeedSecondLevel();
        @NotNull final Properties properties = new Properties();
        Optional.ofNullable(dialect).orElseThrow(() ->
                new ConnectionFailedException("Dialect")
        );
        Optional.ofNullable(hbm2ddl).orElseThrow(() ->
                new ConnectionFailedException("Hbm2ddl")
        );
        Optional.ofNullable(showSql).orElseThrow(() ->
                new ConnectionFailedException("ShowSql")
        );
        Optional.ofNullable(secondLevel).orElseThrow(() ->
                new ConnectionFailedException("SecondLevel")
        );
        Optional.ofNullable(queryCache).orElseThrow(() ->
                new ConnectionFailedException("QueryCache")
        );
        Optional.ofNullable(minimalPuts).orElseThrow(() ->
                new ConnectionFailedException("MinimalPuts")
        );
        Optional.ofNullable(liteMember).orElseThrow(() ->
                new ConnectionFailedException("LiteMember")
        );
        Optional.ofNullable(regionPrefix).orElseThrow(() ->
                new ConnectionFailedException("RegionPrefix")
        );
        Optional.ofNullable(providerConfiguration).orElseThrow(() ->
                new ConnectionFailedException("ProviderConfiguration")
        );
        Optional.ofNullable(factoryClass).orElseThrow(() ->
                new ConnectionFailedException("FactoryClass")
        );
        properties.put(Environment.DIALECT, dialect);
        properties.put(Environment.HBM2DDL_AUTO, hbm2ddl);
        properties.put(Environment.SHOW_SQL, showSql);
        if(Optional.ofNullable(needSecondLevel).isPresent() &&
                Boolean.parseBoolean(needSecondLevel)){
            properties.put(Environment.USE_SECOND_LEVEL_CACHE, secondLevel);
            properties.put(Environment.USE_QUERY_CACHE, queryCache);
            properties.put(Environment.USE_MINIMAL_PUTS, minimalPuts);
            properties.put(Environment.CACHE_REGION_PREFIX, factoryClass);
            properties.put(
                    Environment.CACHE_PROVIDER_CONFIG, providerConfiguration
            );
            properties.put(Environment.CACHE_REGION_FACTORY, regionPrefix);
            properties.put(LITE_MEMBER, liteMember);
        }
        return properties;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull
            final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        final JpaTransactionManager transactionManager =
                new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(
                entityManagerFactory.getObject()
        );
        return transactionManager;
    }

}
