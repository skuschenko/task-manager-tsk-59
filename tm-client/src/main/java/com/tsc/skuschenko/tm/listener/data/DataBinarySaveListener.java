package com.tsc.skuschenko.tm.listener.data;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.DataEndpoint;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    private static final String DESCRIPTION = "save data in binary file";

    private static final String NAME = "data-save-bin";

    @Autowired
    private DataEndpoint dataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBinarySaveListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        dataEndpoint.saveDataBinaryCommand(session);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
