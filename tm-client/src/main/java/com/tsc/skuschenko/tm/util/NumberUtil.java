package com.tsc.skuschenko.tm.util;

import org.jetbrains.annotations.NotNull;

public interface NumberUtil {

    @NotNull
    static String formatSize(final long bytes) {
        @NotNull final String[] units =
                {"B", "kB", "MB", "GB", "TB", "PB", "EB"};
        final int base = 1024;
        if (bytes < base) {
            return bytes + " " + units[0];
        }
        final int exponent = (int) (Math.log(bytes) / Math.log(base));
        @NotNull final String unit = units[exponent];
        return String.format("%.2f %s", bytes / Math.pow(base, exponent), unit);
    }

}
