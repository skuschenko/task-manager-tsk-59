package com.tsc.skuschenko.tm.listener.system;

import com.tsc.skuschenko.tm.api.service.IListenerService;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public final class AllListenersShowListener extends AbstractListener {

    private static final String DESCRIPTION = "commands";

    private static final String NAME = "commands";

    @Autowired
    private IListenerService commandService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(
            condition = "@allListenersShowListener.name() == #event.name"
    )
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        @NotNull final Collection<String> names =
                commandService.getListListenerNames();
        names.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(System.out::println);
    }

    @Override
    public String name() {
        return NAME;
    }

}
