package com.tsc.skuschenko.tm.listener.user;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.UserEndpoint;
import com.tsc.skuschenko.tm.event.ConsoleEvent;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class UserUnlockByLoginListener extends AbstractUserListener {

    private static final String DESCRIPTION = "unlock user by login";

    private static final String NAME = "unlock-user-by-login";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(
            condition = "@userUnlockByLoginListener.name() == #event.name"
    )
    public void handler(@NotNull final ConsoleEvent event) {
        showOperationInfo(NAME);
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showParameterInfo("login");
        @NotNull final String login = TerminalUtil.nextLine();
        userEndpoint.unlockUserByLogin(session, login);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
