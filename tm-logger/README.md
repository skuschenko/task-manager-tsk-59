# Task Manager Logger

Console Application

# User Info

NAME: Semyon Kuschenko

EMAIL: skushchenko@tsconsulting

COMPANY: tsc

# Software

* JDK 1.8

* OS Windows 

# Hardware

* RAM 16GB

* CPU i7

* HDD 128GB

# Build Programm

```
mvn clean install

```

# Run Program

```
java -jar ./task-manager-logger.jar

```

# Run Program with Docker

```
docker run -p 8080:8080 tm-logger

```

# Screenshots
SCREENFOLDER: https://yadi.sk/d/1P5tffaZGLQ2hA?w=1
